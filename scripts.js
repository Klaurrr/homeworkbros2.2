const loadBtn = document.querySelector(".js-load");
const resultsContainer = document.querySelector(".js-results");
const searchInput = document.querySelector(".js-input");

loadBtn.addEventListener("click", function (e) {
  e.preventDefault();
  const searchValue = searchInput.value.trim().toLowerCase();
  try {
    if (isNaN(searchValue)) {
        throw new Error('Вы ввели строку')
    } else if (searchValue.length === 0) {
      throw new Error('Поле пустое')
    } else if (!isNaN(searchValue)) {
        if (searchValue < 5 || searchValue > 10) {
          throw new Error('Введите значение в диапозоне от 5 до 10')
        } else (resultsContainer.innerHTML = `<div class="response-container">
        <p><span>${searchValue}</span><p>
    </div>`)
    }

  } catch (e) {
    (resultsContainer.innerHTML = `<div class="response-container">
    <p><span>${e}</span><p>
</div>`)
  }
});

async function lottery() {
  try {
    console.log("Вы начали игру");
    let promise = await new Promise(function (resolve, reject) {
      setTimeout(function () {
        Math.random(0) > 0.5 ? resolve() : reject("Вы промахнулись");
      }, 1000);
    });
    console.log("Вы выиграли");
    console.log("Вам заплатили");
  } catch (e) {
    console.log(e)
    console.log("Вы проиграли");
  } finally {
    console.log("Игра окончена");
  }
}

lottery();


